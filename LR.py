import numpy as np
import math

from scipy import sparse
from scipy import special

class LR(object):
	def __init__(self, method:str = 'NTD', C:float = 0.1, eta:float = 0.01, xi:float = 0.1, epsilon:float = 0.01):
		self.method = method
		self.C = C
		self.eta = eta
		self.xi = xi
		self.epsilon = epsilon
		self.W = 0
		self.PosLbl = 0
		self.NegLbl = 0

	def Grad(self, X, Y):
		w = sparse.diags([self.C * np.multiply((special.expit(np.multiply(Y, X.dot(self.W))) - 1), Y)], [0])
		return self.W + (w * X).sum(0).A1

	def Func(self, X, Y):
		return wtw / 2 + self.C * np.sum(np.log1p(np.exp(-np.multiply(Y, wtx))))

	def GD(self, X, Y, msg:bool = True):
		grad = self.Grad(X, Y)
		ng = np.linalg.norm(grad)
		g0 = ng
		Itr = 0
		while True:
			if ng <= self.epsilon * g0:
				break
			s = -grad
			wtw = np.dot(self.W, self.W)
			wtsx2 = np.dot(self.W, s) * 2
			sts = np.dot(s, s)
			wtx = X.dot(self.W)
			stx = X.dot(s)
			gts = self.eta * np.dot(grad, s)
			f = wtw / 2 + self.C * np.sum(np.log1p(np.exp(-np.multiply(Y, wtx))))
			a = 1
			while True:
				nf = (wtw + a * wtsx2 + a * a * sts) / 2 + self.C * np.sum(np.log1p(np.exp(-np.multiply(Y, wtx + a * stx))))
				tf = f + a * gts
				if nf < tf:
					break;
				a /= 2
			self.W += s * a
			Itr += 1	
			if msg:
				print('iter  ', Itr, ' f ', f, ' |g| ', ng, ' step_size ', a)
			grad = self.Grad(X, Y)
			ng = np.linalg.norm(grad)
	
	def NT(self, X, Y, msg:bool = True):
		grad = self.Grad(X, Y)
		ng = np.linalg.norm(grad)
		g0 = ng
		Itr = 0
		while True:
			if ng <= self.epsilon * g0:
				break
			wtw = np.dot(self.W, self.W)
			wtx = X.dot(self.W)
			r = -grad
			d = r
			s = np.zeros(X.shape[1])

			eywtx = np.exp(-np.multiply(Y, wtx))
			D = np.divide(eywtx, np.square(eywtx + 1))
			nr = np.linalg.norm(r)
			ncg = 0
			while True:
				if nr <= self.xi * ng:
					break			
				hd = d + self.C * X.transpose().dot(np.multiply(D, X.dot(d)))
				a = nr * nr / np.dot(d, hd)
				s = s + a * d
				onr = nr
				r = r - a * hd
				nr = np.linalg.norm(r)
				b = nr * nr / (onr * onr)
				d = r + b * d
				ncg += 1
		
			wtsx2 = np.dot(self.W, s) * 2
			sts = np.dot(s, s)	
			stx = X.dot(s)
			gts = self.eta * np.dot(grad, s)
			f = wtw / 2 + self.C * np.sum(np.log1p(np.exp(-np.multiply(Y, wtx))))
			a = 1
			while True:
				nf = (wtw + a * wtsx2 + a * a * sts) / 2 + self.C * np.sum(np.log1p(np.exp(-np.multiply(Y, wtx + a * stx))))
				tf = f + a * gts
				if nf < tf:
					break;
				a /= 2
			self.W += s * a
			Itr += 1
			if msg:
				print('iter  ', Itr, ' f ', f, ' |g| ', ng, ' CG   ', ncg, ' step_size ', a)
			grad = self.Grad(X, Y)
			ng = np.linalg.norm(grad)	

	def fit(self, X, Y, msg:bool = True):
		self.W = np.array([0] * X.shape[1], dtype = float)
		ref = Y[0]
		for i in range(len(Y)):
			if Y[i] == ref:
				self.PosLbl = Y[i]
				Y[i] = 1
			else:
				self.NegLbl = Y[i]
				Y[i] = -1
		
		if self.method == 'GD':
			self.GD(X, Y)
		else:
			self.NT(X, Y)

	def predict(self, X):
		if X.shape[1] > len(self.W):
			T = X[:, :len(self.W)]
		else:
			T = X
		ret = special.expit(T.dot(self.W[:X.shape[1]]))
		for i in range(len(ret)):
			if ret[i] > 0.5:
				ret[i] = self.PosLbl
			else:
				ret[i] = self.NegLbl
		return ret
			
