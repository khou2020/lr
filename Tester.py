import os, sys
import numpy as np

import LR

from sklearn import datasets
from scipy import sparse
from sklearn import metrics

def main(argc:int, argv:list):
	if argc > 3:
		model = LR.LR(method = argv[3])
	else:
		model = LR.LR()

	print('Loading Data')
	TX, TY = datasets.load_svmlight_file(argv[1])
	VX, VY = datasets.load_svmlight_file(argv[2])

	print('Training')
	model.fit(TX, TY)

	print('Evaluating')
	Pred = model.predict(VX)
	acc = metrics.accuracy_score(VY, Pred)
	print('Accuracy: ' + str(acc))

	return 0

if __name__ == '__main__':
	main(len(sys.argv), sys.argv)